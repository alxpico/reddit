package android.rico.reddit;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RedditPostTask extends AsyncTask<RedditListFragment, Void, JSONObject> {
    private RedditListFragment redditListFragment;

    @Override
    protected JSONObject doInBackground(RedditListFragment... redditListFragments) {
        JSONObject jsonobject = null;
        redditListFragment = redditListFragments[0];

        try {
            URL redditFeedURL = new URL("https://reddit.com/r/android.json");

            HttpURLConnection httpConnection = (HttpURLConnection) redditFeedURL.openConnection();
            httpConnection.connect();

            int statusCode = httpConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_OK) {
               jsonobject = RedditPostParser.getInstance().parserInputStream(httpConnection.getInputStream());
            }
        } catch (MalformedURLException error) {
            Log.e("RedditPostTask", "MalformedURLException (doInBackfround): " + error);
        } catch (IOException error) {
            Log.e("RedditPostTask ", "IOException (doInBackground): " + error);
        }

        return jsonobject;

    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
         RedditPostParser.getInstance().readRedditFeed(jsonObject);
        redditListFragment.updateUserInterface();
    }
}
