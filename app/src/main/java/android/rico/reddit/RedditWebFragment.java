package android.rico.reddit;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class RedditWebFragment extends Fragment {

    private final static String POST_URL = "1";
    private String redditUrl;

    public static RedditWebFragment newFragment(String redditUrl) {
        Bundle argument = new Bundle();
        argument.putSerializable(POST_URL, redditUrl);

        RedditWebFragment fragment = new RedditWebFragment();
        fragment.setArguments(argument);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redditUrl = getArguments().getString(POST_URL);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutVeiw = inflater.inflate(R.layout.fragment_reddit_web, container, false);


        WebView webView = (WebView)layoutVeiw.findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(redditUrl);

        return layoutVeiw;
    }
}
